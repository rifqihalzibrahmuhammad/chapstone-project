<?php

use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\AdminPeopleController;
use App\Http\Controllers\AdminReportController;
use App\Http\Controllers\AdminUserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GenerateController;
use App\Http\Controllers\IdentifyController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReportController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/test', function () {
    return Inertia::render('Test');
});

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');

    Route::get('/generate', [GenerateController::class, 'index'])->name('generate.index');

    Route::match(['get', 'post'], '/identify', [IdentifyController::class, 'index'])->name('identify.index');

    Route::get('/report', [ReportController::class, 'index'])->name('report.index');
    Route::post('/report', [ReportController::class, 'store'])->name('report.store');
    Route::get('/report/{report}/generate', [ReportController::class, 'generate'])->name('report.generate');

    Route::prefix('admin')->name('admin.')->middleware(['admin'])->group(function () {
        Route::get('/', [AdminDashboardController::class, 'index'])->name('dashboard.index');

        Route::get('/profile', [ProfileController::class, 'adminEdit'])->name('profile.edit');

        Route::get('/user', [AdminUserController::class, 'index'])->name('user.index');
        Route::get('/user/create', [AdminUserController::class, 'create'])->name('user.create');
        Route::post('/user', [AdminUserController::class, 'store'])->name('user.store');
        Route::get('/user/{user}/edit', [AdminUserController::class, 'edit'])->name('user.edit');
        Route::patch('/user/{user}', [AdminUserController::class, 'update'])->name('user.update');
        Route::delete('/user', [AdminUserController::class, 'destroy'])->name('user.destroy');

        Route::get('/people', [AdminPeopleController::class, 'index'])->name('people.index');
        Route::get('/people/create', [AdminPeopleController::class, 'create'])->name('people.create');
        Route::post('/people', [AdminPeopleController::class, 'store'])->name('people.store');
        Route::get('/people/{people}/edit', [AdminPeopleController::class, 'edit'])->name('people.edit');
        Route::post('/people/{people}', [AdminPeopleController::class, 'update'])->name('people.update');
        Route::delete('/people', [AdminPeopleController::class, 'destroy'])->name('people.destroy');

        Route::get('/report', [AdminReportController::class, 'index'])->name('report.index');
        Route::get('/report/create', [AdminReportController::class, 'create'])->name('report.create');
        Route::get('/report/{report}/approve', [AdminReportController::class, 'approve'])->name('report.approve');
        Route::patch('/report/{report}', [AdminReportController::class, 'update'])->name('report.update');
    });
});

require __DIR__ . '/auth.php';

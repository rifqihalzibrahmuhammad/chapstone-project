import Authenticated from "@/Layouts/AuthenticatedLayout"
import { Head } from "@inertiajs/react"

export default function Index() {
    return (
        <>
            <Head title="Dashboard" />

            <Authenticated>

            </Authenticated>
        </>
    )
}
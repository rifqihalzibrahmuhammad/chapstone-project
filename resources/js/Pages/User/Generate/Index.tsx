import Authenticated from "@/Layouts/AuthenticatedLayout"
import { zodResolver } from "@hookform/resolvers/zod"
import { Head, router, usePage } from "@inertiajs/react"
import { useForm } from "react-hook-form"
import { z } from "zod"

import {
    CornerDownLeft,
    LoaderCircle,
    Mic,
    Paperclip
} from "lucide-react"

import { Badge } from "@/Components/ui/badge"
import { Button } from "@/Components/ui/button"
import { Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage } from "@/Components/ui/form"
import { Label } from "@/Components/ui/label"
import { RadioGroup, RadioGroupItem } from "@/Components/ui/radio-group"
import { ScrollArea } from "@/Components/ui/scroll-area"
import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue
} from "@/Components/ui/select"
import { Textarea } from "@/Components/ui/textarea"
import { Tooltip, TooltipContent, TooltipTrigger } from "@/Components/ui/tooltip"
import { Skeleton } from "@/Components/ui/skeleton"
import axios, { AxiosResponse } from "axios"
import { useState } from "react"

const FormSchema = z.object({
    gender: z.string({ required_error: "Please select gender to display.", }),
    age: z.string({ required_error: "Please select age range to display.", }),
    skin: z.string({ required_error: "Please select skin color to display.", }),
    face: z.string({ required_error: "Please select face shape to display.", }),
    hairType: z.string({ required_error: "Please select hair type to display.", }),
    hairColor: z.string({ required_error: "Please select hair color to display.", }),
    forehead: z.string({ required_error: "Please select forehead shape to display.", }),
    eyebrow: z.string({ required_error: "Please select eyebrow shape to display.", }),
    eyeShape: z.string({ required_error: "Please select eye shape to display.", }),
    eyeColor: z.string({ required_error: "Please select eye color to display.", }),
    nose: z.string({ required_error: "Please select nose shape to display.", }),
    lip: z.string({ required_error: "Please select lip shape to display.", }),
    chin: z.string({ required_error: "Please select chin shape to display.", }),
    ear: z.string({ required_error: "Please select ear shape to display.", }),
    prompt: z.string({ message: "Prompt must be at least 30 characters.", }),
    // eyeAbnormal: z.string({ required_error: "Please select eye abnormalities to display.", }),
    // body: z.string({ required_error: "Please select body shape to display.", }),
})

export default function Index() {
    const [image, updateImage] = useState<ImageData | undefined>(undefined);
    const [prompt, updatePrompt] = useState<string | undefined>(undefined);
    const [generateLoading, updateGenerateLoading] = useState<boolean>(false);
    const [identifyLoading, updateIdentifyLoading] = useState<boolean>(false);

    const form = useForm<z.infer<typeof FormSchema>>({
        resolver: zodResolver(FormSchema),
    })

    const submitGenerate = async (value: z.infer<typeof FormSchema>) => {
        updateGenerateLoading(true);

        const promptString = `A face with highly detailed, photorealistic portrait, facing front, ${value.gender}, ${value.age}, ${value.skin} skin, ${value.face} face, ${value.hairType} ${value.hairColor} hair, ${value.forehead} forehead, ${value.eyebrow} eyebrow, ${value.eyeShape} eyes shape, ${value.eyeColor} eyes color, ${value.nose} nose, ${value.lip} lip, ${value.chin} chin, ${value.ear} ears, ${value.prompt}`;

        updatePrompt(promptString);

        const formData = new FormData();
        formData.append('prompt', promptString);

        try {
            const response: AxiosResponse<ImageData> = await axios.post<ImageData>('http://127.0.0.1:8000/api/generate', formData);
            updateImage(response.data);
            updateGenerateLoading(false);
        } catch (error) {
            console.error("Error fetching image:", error);
            updateGenerateLoading(false);
        }
    }

    const submitIdentify = async () => {
        updateIdentifyLoading(true);

        if (!image) return;
        const formData = new FormData();
        const imageBlob = await convertImageDataToBlob(image);
        formData.append('image', imageBlob);
        formData.append('prompt', prompt);

        try {
            const response = await axios.post('http://127.0.0.1:8000/api/identify', formData);

            if (response.status != 200) {
                throw new Error(`API request failed with status ${response.status}`);
            }

            router.visit(route("identify.index"), {
                method: "post",
                data: response.data,
            });

            updateIdentifyLoading(false);
        } catch (error) {
            console.error(error);
            // Handle errors appropriately (e.g., display user-friendly messages)
            updateIdentifyLoading(false);
        }
    };

    const convertImageDataToBlob = async (imageData: ImageData): Promise<Blob> => {
        return new Promise((resolve, reject) => {
            const img = new Image();
            img.onload = () => {
                const canvas = document.createElement('canvas');
                canvas.width = img.width;
                canvas.height = img.height;
                const ctx = canvas.getContext('2d');
                if (!ctx) {
                    reject(new Error('Canvas context not supported'));
                    return;
                }
                ctx.drawImage(img, 0, 0);
                const newImageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                canvas.toBlob((blob) => {
                    if (blob) {
                        resolve(blob);
                    } else {
                        reject(new Error('Failed to convert ImageData to Blob'));
                    }
                });
            };
            img.onerror = () => {
                reject(new Error('Failed to load image'));
            };
            img.src = `data:image/png;base64,${imageData}`;
        });
    };

    return (
        <>
            <Head title="Generate" />

            <Authenticated>
                <Form {...form}>
                    <form onSubmit={form.handleSubmit(submitGenerate)}>
                        <div className="grid flex-1 gap-4 overflow-auto p-4 md:grid-cols-2 lg:grid-cols-7">
                            <div
                                className="relative hidden flex-col items-start gap-8 md:flex lg:col-span-3" x-chunk="dashboard-03-chunk-0"
                            >
                                <div className="grid w-full items-start gap-6">
                                    <fieldset className="grid gap-6 rounded-lg border p-4">
                                        <legend className="-ml-1 px-1 text-sm font-medium">
                                            Main
                                        </legend>
                                        <div className="grid gap-3">
                                            <FormField
                                                control={form.control}
                                                name="gender"
                                                render={({ field }) => (
                                                    <FormItem>
                                                        <FormLabel>Gender</FormLabel>
                                                        <Select onValueChange={field.onChange} defaultValue={field.value}>
                                                            <FormControl>
                                                                <SelectTrigger>
                                                                    <SelectValue placeholder="Select gender to display" />
                                                                </SelectTrigger>
                                                            </FormControl>
                                                            <SelectContent>
                                                                <SelectItem value="male">Male</SelectItem>
                                                                <SelectItem value="female">Female</SelectItem>
                                                            </SelectContent>
                                                        </Select>
                                                        <FormMessage />
                                                    </FormItem>
                                                )}
                                            />

                                            <FormField
                                                control={form.control}
                                                name="age"
                                                render={({ field }) => (
                                                    <FormItem>
                                                        <FormLabel>Age</FormLabel>
                                                        <Select onValueChange={field.onChange} defaultValue={field.value}>
                                                            <FormControl>
                                                                <SelectTrigger>
                                                                    <SelectValue placeholder="Select age to display" />
                                                                </SelectTrigger>
                                                            </FormControl>
                                                            <SelectContent>
                                                                <SelectItem value="teen (15-19 years)">Teen (15-19 years)</SelectItem>
                                                                <SelectItem value="young adult (20-30 years)">Young Adult (20-30 years)</SelectItem>
                                                                <SelectItem value="adult (31-45 years)">Adult (31-45 years)</SelectItem>
                                                                <SelectItem value="middle aged (46-60 years)">Middle-Aged (46-60 years)</SelectItem>
                                                                <SelectItem value="senior (61-75 years)">Senior (61-75 years)</SelectItem>
                                                                <SelectItem value="elderly (76+ years)">Elderly (76+ years)</SelectItem>
                                                            </SelectContent>
                                                        </Select>
                                                        <FormMessage />
                                                    </FormItem>
                                                )}
                                            />
                                        </div>
                                    </fieldset>
                                    <fieldset className="grid gap-6 rounded-lg border p-4">
                                        <legend className="-ml-1 px-1 text-sm font-medium">
                                            Detail
                                        </legend>
                                        <ScrollArea className="h-[22rem]">
                                            <div className="grid gap-3">
                                                <FormField control={form.control} name="face" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Face Shapes</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'oval' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="oval" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/1.png" alt="Oval face" className="h-28" />
                                                                        <div className="mt-2">Oval</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'square' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="square" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/2.png" alt="Square face" className="h-28" />
                                                                        <div className="mt-2">Square</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'round' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="round" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/3.png" alt="Round face" className="h-28" />
                                                                        <div className="mt-2">Round</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'heart' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="heart" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/4.png" alt="Heart face" className="h-28" />
                                                                        <div className="mt-2">Heart</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'oblong' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="oblong" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/5.png" alt="Oblong face" className="h-28" />
                                                                        <div className="mt-2">Oblong</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'diamond' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="diamond" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/6.png" alt="Diamond face" className="h-28" />
                                                                        <div className="mt-2">Diamond</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="skin" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Skin Tones</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'light pale white' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="light pale white" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/skin/1.png" alt="Type 1" className="h-28" />
                                                                        <div className="mt-2">Type 1</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'white fair' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="white fair" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/skin/2.png" alt="Type 2" className="h-28" />
                                                                        <div className="mt-2">Type 2</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'medium white to olive' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="medium white to olive" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/skin/3.png" alt="Type 3" className="h-28" />
                                                                        <div className="mt-2">Type 3</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'olive moderate brown' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="olive moderate brown" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/skin/4.png" alt="Type 4" className="h-28" />
                                                                        <div className="mt-2">Type 4</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'brown to dark brown' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="brown to dark brown" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/skin/5.png" alt="Type 5" className="h-28" />
                                                                        <div className="mt-2">Type 5</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'black very dark brown to black' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="black very dark brown to black" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/skin/6.png" alt="Type 6" className="h-28" />
                                                                        <div className="mt-2">Type 6</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="hairType" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Hair Types</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'straight' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="straight" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/hair style/1.png" alt="Straight hair" className="h-28" />
                                                                        <div className="mt-2">Straight</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'wavy' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="wavy" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/hair style/2.png" alt="Wavy hair" className="h-28" />
                                                                        <div className="mt-2">Wavy</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'curly' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="curly" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/hair style/3.png" alt="Curly hair" className="h-28" />
                                                                        <div className="mt-2">Curly</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'kinky' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="kinky" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/hair style/4.png" alt="Kinky hair" className="h-28" />
                                                                        <div className="mt-2">Kinky</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="hairColor" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Hair Colors</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'black' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="black" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/5.png" alt="Black hair" className="h-28" />
                                                                        <div className="mt-2">Black</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'brown' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="brown" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/5.png" alt="Brown hair" className="h-28" />
                                                                        <div className="mt-2">Brown</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'blonde' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="blonde" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/5.png" alt="Blonde hair" className="h-28" />
                                                                        <div className="mt-2">Blonde</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'white or gray' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="white or gray" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/5.png" alt="White/Gray hair" className="h-28" />
                                                                        <div className="mt-2">White/Gray</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'red' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="red" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/face shape/5.png" alt="Red hair" className="h-28" />
                                                                        <div className="mt-2">Red</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="forehead" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Forehead Types</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'broad' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="broad" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/forehead/1.png" alt="Broad forehead" className="h-28" />
                                                                        <div className="mt-2">Broad</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'narrow' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="narrow" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/forehead/2.png" alt="Narrow forehead" className="h-28" />
                                                                        <div className="mt-2">Narrow</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'curved' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="curved" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/forehead/3.png" alt="Curved forehead" className="h-28" />
                                                                        <div className="mt-2">Curved</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'm shaped' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="m shaped" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/forehead/4.png" alt="M-Shaped forehead" className="h-28" />
                                                                        <div className="mt-2">M-Shaped</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="eyebrow" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Eyebrow Types</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'thick' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="thick" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye brow/1.png" alt="Thick eyebrow" className="h-28" />
                                                                        <div className="mt-2">Thick</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'thin' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="thin" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye brow/2.png" alt="Thin eyebrow" className="h-28" />
                                                                        <div className="mt-2">Thin</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'arched' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="arched" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye brow/3.png" alt="Arched eyebrow" className="h-28" />
                                                                        <div className="mt-2">Arched</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'straight' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="straight" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye brow/4.png" alt="Straight eyebrow" className="h-28" />
                                                                        <div className="mt-2">Straight</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="eyeShape" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Eye Shapes</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'round' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="round" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eyes/1.png" alt="Round eyes" className="h-28" />
                                                                        <div className="mt-2">Round</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'monolid' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="monolid" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eyes/2.jpeg" alt="Monoloid eyes" className="h-28" />
                                                                        <div className="mt-2">Monolid</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'down-turned' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="down-turned" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eyes/3.png" alt="Down-Turned eyes" className="h-28" />
                                                                        <div className="mt-2">Down-Turned</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'hooded' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="hooded" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eyes/4.png" alt="Hooded eyes" className="h-28" />
                                                                        <div className="mt-2">Hooded</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'almond' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="almond" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eyes/5.png" alt="Almond eyes" className="h-28" />
                                                                        <div className="mt-2">Almond</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="eyeColor" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Eye Colors</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'black' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="black" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye color/1.jpg" alt="Brown eyes" className="h-28" />
                                                                        <div className="mt-2">Brown</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'amber' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="amber" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye color/2.jpg" alt="Amber eyes" className="h-28" />
                                                                        <div className="mt-2">Amber</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'green' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="green" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye color/3.jpg" alt="Green eyes" className="h-28" />
                                                                        <div className="mt-2">Green</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'hazel' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="hazel" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye color/4.jpg" alt="Hazel eyes" className="h-28" />
                                                                        <div className="mt-2">Hazel</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'blue' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="blue" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye color/5.jpg" alt="Blue eyes" className="h-28" />
                                                                        <div className="mt-2">Blue</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'gray' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="gray" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/eye color/6.jpg" alt="Gray eyes" className="h-28" />
                                                                        <div className="mt-2">Gray</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="nose" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Nose Shapes</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'straight' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="straight" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/nose/1.jpg" alt="Straight Nose" className="h-28" />
                                                                        <div className="mt-2">Straight</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'crooked' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="crooked" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/nose/2.jpg" alt="Crooked nose" className="h-28" />
                                                                        <div className="mt-2">Crooked</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'snub' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="snub" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/nose/3.jpg" alt="Snub nose" className="h-28" />
                                                                        <div className="mt-2">Snub</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'big' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="big" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/nose/4.jpg" alt="Big nose" className="h-28" />
                                                                        <div className="mt-2">Big</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'sharp' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="sharp" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/nose/5.jpg" alt="Sharp Nose" className="h-28" />
                                                                        <div className="mt-2">Sharp</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'rounded' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="rounded" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/nose/6.jpg" alt="Rounded nose" className="h-28" />
                                                                        <div className="mt-2">Rounded</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="lip" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Lip Shapes</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'normal' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="normal" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/mouth/3.png" alt="Normal lip" className="h-28" />
                                                                        <div className="mt-2">Normal</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'thin' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="thin" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/mouth/6.png" alt="Thin lip" className="h-28" />
                                                                        <div className="mt-2">Thin</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'thick' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="thick" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/mouth/1.png" alt="Thick lip" className="h-28" />
                                                                        <div className="mt-2">Thick</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'heavy upper' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="heavy upper" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/mouth/2.png" alt="Heavy upper lip" className="h-28" />
                                                                        <div className="mt-2">Heavy Upper</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'heavy lower' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="heavy lower" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/mouth/4.png" alt="Heavy lower lip" className="h-28" />
                                                                        <div className="mt-2">Heavy Lower</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="chin" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Chin Shapes</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'square' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="square" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/chin/1.png" alt="Square chin" className="h-28" />
                                                                        <div className="mt-2">Square</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'rounded' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="rounded" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/chin/2.png" alt="Rounded chin" className="h-28" />
                                                                        <div className="mt-2">Rounded</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'cleft' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="cleft" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/chin/3.png" alt="Cleft chin" className="h-28" />
                                                                        <div className="mt-2">Cleft</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'receding' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="receding" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/chin/4.png" alt="Receding chin" className="h-28" />
                                                                        <div className="mt-2">Receding</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'double' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="double" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/chin/6.png" alt="Double chin" className="h-28" />
                                                                        <div className="mt-2">Double</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />

                                                <FormField control={form.control} name="ear" render={({ field }) => (
                                                    <FormItem className="space-y-3">
                                                        <FormLabel>Ear Shapes</FormLabel>
                                                        <FormControl>
                                                            <RadioGroup
                                                                onValueChange={field.onChange}
                                                                defaultValue={field.value}
                                                                className="grid grid-cols-2 gap-4 lg:grid-cols-3"
                                                            >
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'attached lobe' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="attached lobe" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/ear/1.png" alt="Attached lobe ear" className="h-28" />
                                                                        <div className="mt-2">Attached lobe</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'broad lobe' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="broad lobe" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/ear/2.png" alt="Broad lobe ear" className="h-28" />
                                                                        <div className="mt-2">Broad lobe</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'narrow' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="narrow" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/ear/3.png" alt="Narrow ear" className="h-28" />
                                                                        <div className="mt-2">Narrow</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'pointed' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="pointed" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/ear/4.png" alt="Pointed ear" className="h-28" />
                                                                        <div className="mt-2">Pointed</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'round' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="round" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/ear/5.png" alt="Round ear" className="h-28" />
                                                                        <div className="mt-2">Round</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'square' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="square" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/ear/6.png" alt="Square ear" className="h-28" />
                                                                        <div className="mt-2">Square</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                                <FormItem
                                                                    className={`flex items-center justify-center flex-col space-y-3 px-2 py-1 rounded ${field.value === 'sticking out' ? 'border-2 border-zinc-400' : ''
                                                                        }`}
                                                                >
                                                                    <FormControl>
                                                                        <RadioGroupItem className="hidden" value="sticking out" />
                                                                    </FormControl>
                                                                    <FormLabel className="font-normal text-center">
                                                                        <img src="https://test-chapstone.s3.amazonaws.com/assets/ear/7.png" alt="Sticking out ear" className="h-28" />
                                                                        <div className="mt-2">Sticking out</div>
                                                                    </FormLabel>
                                                                </FormItem>
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <FormMessage />
                                                    </FormItem>
                                                )} />
                                            </div>
                                        </ScrollArea>
                                    </fieldset>
                                </div>
                            </div>
                            <div className="relative flex h-full min-h-[50vh] flex-col rounded-xl bg-muted/50 p-4 lg:col-span-4">
                                <Badge variant="outline" className="absolute right-3 top-3">
                                    Output
                                </Badge>
                                <div className="flex flex-col items-center justify-center flex-1">
                                    {generateLoading ? (
                                        <Skeleton className="h-[50vh] w-[80%] max-w-[550px] rounded-xl" />
                                    ) : image ? (
                                        <img
                                            src={`data:image/png;base64,${image}`}
                                            className="shadow-lg max-w-full max-h-[50vh] object-contain"
                                        />
                                    ) : null}
                                </div>
                                <div
                                    className="relative overflow-hidden rounded-lg border bg-background focus-within:ring-1 focus-within:ring-ring" x-chunk="dashboard-03-chunk-1"
                                >
                                    <FormField
                                        control={form.control}
                                        name="prompt"
                                        render={({ field }) => (
                                            <FormItem>
                                                <FormLabel className="hidden">Message</FormLabel>
                                                <FormControl>
                                                    <Textarea
                                                        placeholder="Type your additional details..."
                                                        className="min-h-12 resize-none border-0 p-3 shadow-none focus-visible:ring-0"
                                                        {...field}
                                                    />
                                                </FormControl>
                                                <FormDescription className="hidden">
                                                    You can <span>@mention</span> other users and organizations.
                                                </FormDescription>
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                    <div className="flex items-center p-3 pt-0">
                                        {generateLoading || identifyLoading ? (
                                            <Button type="button" size="sm" className=" ml-auto gap-1.5">
                                                <LoaderCircle className="animate-spin" />
                                                Processing ...
                                            </Button>
                                        ) : (
                                            <Button type="submit" size="sm" className="ml-auto gap-1.5">
                                                Generate
                                                <CornerDownLeft className="size-3.5" />
                                            </Button>
                                        )}

                                        {image ? (
                                            generateLoading || identifyLoading ? (
                                                null
                                            ) : (
                                                <Button type="button" size="sm" onClick={submitIdentify} className="ms-2">
                                                    Identify
                                                </Button>
                                            )
                                        ) : null}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </Form>
            </Authenticated>
        </>
    )
}
import InputError from "@/Components/InputError"
import { Badge } from "@/Components/ui/badge"
import { Button } from "@/Components/ui/button"
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/Components/ui/card"
import { Form, FormField, FormItem } from "@/Components/ui/form"
import { Input } from "@/Components/ui/input"
import { Label } from "@/Components/ui/label"
import { ScrollArea } from "@/Components/ui/scroll-area"
import { Textarea } from "@/Components/ui/textarea"
import Authenticated from "@/Layouts/AuthenticatedLayout"
import { Head, useForm } from "@inertiajs/react"
import axios from "axios"
import { AxiosResponse } from "axios"
import { CornerDownLeft, LoaderCircle } from "lucide-react"
import { useEffect, useState } from "react"
import { S3Client, PutObjectCommand } from "@aws-sdk/client-s3";
import { v4 as uuidv4 } from 'uuid';

export default function Index(props) {
    const { identify_response } = props
    const [imageData, setImageData] = useState<ImageData | undefined>(undefined);
    const [loading, setLoading] = useState<boolean>(false);

    const newList = identify_response.results.map(item => {
        // Perform any transformations or modifications you need here
        // For example, if each item in the data is an object and you want to extract a specific property:
        // const newItem = item.propertyName;
        return item; // Replace this with any modifications you want to apply to each item
    });

    const { data, setData, post, errors } = useForm({
        image: null as Blob | null,
        prompt: identify_response.prompt,
        title: "",
        description: "",
        details: newList,
    });

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response: AxiosResponse<string> = await axios.get<string>(
                    'http://127.0.0.1:8000/api/getimage'
                );
                setImageData(response.data); // Assuming data is the image itself

                // Move the logic that depends on imageData here
                if (response.data) {
                    const imageBlob = await convertImageDataToBlob(response.data);
                    setData('image', imageBlob);
                }
            } catch (error) {
                console.error('Error fetching image data:', error);
            }
        };

        fetchData();
    }, []);

    useEffect(() => {
        const handleBeforeUnload = (event) => {
            const e = event || window.event;
            e.preventDefault();
            if (e) {
                e.returnValue = ''; // Legacy method for cross browser support
            }
            return ''; // Legacy method for cross browser support
        };

        window.addEventListener('beforeunload', handleBeforeUnload);

        return () => {
            window.removeEventListener('beforeunload', handleBeforeUnload);
        };
    }, []);

    const s3Client = new S3Client({
        region: "us-east-1",
        credentials: {
            accessKeyId: "AKIAXMI3O6KVUQIHRYOQ",
            secretAccessKey: "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",
        },
    });

    // const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    //     e.preventDefault();
    //     setLoading(true);
    //     if (!imageData) return;

    //     // Convert the ImageData to a Blob
    //     const imageBlob = await convertImageDataToBlob(imageData);

    //     try {
    //         // Generate a unique ID using uuid
    //         const uniqueId = uuidv4();
    //         const key = `generated_sketch/${Date.now()}-${uniqueId}.jpeg`;

    //         // Upload the image to S3
    //         const uploadParams = {
    //             Bucket: "test-chapstone",
    //             Key: key, // Unique key for the image in S3
    //             Body: imageBlob,
    //             ContentType: "image/jpeg",
    //             ACL: "public-read",
    //         };

    //         const uploadResult = await s3Client.send(new PutObjectCommand(uploadParams));
    //         const imageUrl = `https://test-chapstone.s3.amazonaws.com/${key}`;

    //         // Set the image URL in the data state
    //         setData('image', imageUrl);

    //         // Run the post function only if uploadResult is successful and imageUrl is not null
    //         if (imageUrl) {
    //             post(route("report.store"), {
    //                 onSuccess: () => {
    //                     console.log("Form submitted successfully");
    //                     // Handle success actions here
    //                 },
    //                 onError: (errors) => {
    //                     console.error("Form submission error:", errors);
    //                     // Handle error actions here
    //                 },
    //                 data: data,
    //                 preserveScroll: true,
    //             });
    //         }

    //         setLoading(false);
    //     } catch (error) {
    //         console.error("Error uploading image to S3:", error);
    //         // Handle error actions here
    //         setLoading(false);
    //     }
    // };

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        setLoading(true);

        try {
            // Run the post function only if uploadResult is successful and imageUrl is not null
            post(route("report.store"), {
                onSuccess: () => {
                    console.log("Form submitted successfully");
                    // Handle success actions here
                },
                onError: (errors) => {
                    console.error("Form submission error:", errors);
                    // Handle error actions here
                },
                data: data,
                preserveScroll: true,
            });

            setLoading(false);
        } catch (error) {
            console.error("Error uploading image to S3:", error);
            // Handle error actions here
            setLoading(false);
        }
    };

    // const convertImageDataToBlob = async (imageData: ImageData): Promise<Blob> => {
    //     return new Promise((resolve, reject) => {
    //         const img = new Image();
    //         img.onload = () => {
    //             const canvas = document.createElement('canvas');
    //             canvas.width = img.width;
    //             canvas.height = img.height;
    //             const ctx = canvas.getContext('2d');
    //             if (!ctx) {
    //                 reject(new Error('Canvas context not supported'));
    //                 return;
    //             }
    //             ctx.drawImage(img, 0, 0);
    //             const newImageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    //             canvas.toBlob((blob) => {
    //                 if (blob) {
    //                     resolve(blob);
    //                 } else {
    //                     reject(new Error('Failed to convert ImageData to Blob'));
    //                 }
    //             });
    //         };
    //         img.onerror = () => {
    //             reject(new Error('Failed to load image'));
    //         };
    //         img.src = `data:image/png;base64,${imageData}`;
    //     });
    // };

    const convertImageDataToBlob = async (imageData: string): Promise<Blob> => {
        const response = await fetch(`data:image/png;base64,${imageData}`);
        const blob = await response.blob();
        return blob;
    };

    return (
        <>
            <Head title="Identify" />

            <Authenticated>
                <form onSubmit={onSubmit}>
                    <div className="grid flex-1 gap-4 overflow-auto p-4 md:grid-cols-2 lg:grid-cols-3">
                        <div
                            className="relative hidden flex-col items-start gap-8 md:flex lg:col-span-1" x-chunk="dashboard-03-chunk-0"
                        >
                            <div className="grid w-full items-start gap-6">
                                <fieldset className="rounded-lg border p-4 flex flex-col">
                                    {imageData && (
                                        <img src={`data:image/png;base64,${imageData}`} alt="Generated Image" className="aspect-square object-cover rounded-lg overflow-hidden mb-3" />
                                    )}
                                    <div className="text-justify my-2">
                                        {identify_response.prompt}
                                    </div>
                                    <legend className="-ml-1 px-1 text-sm font-medium">Report Details</legend>
                                    <div className="mb-3">
                                        <Label htmlFor="name">Report Title</Label>
                                        <Input
                                            id="title"
                                            type="text"
                                            name="title"
                                            value={data.title}
                                            className="mt-1 block w-full"
                                            placeholder="Enter report title"
                                            autoComplete="title"
                                            onChange={(e) => setData('title', e.target.value)}
                                        />
                                        <InputError message={errors.title} />
                                    </div>
                                    <div className="mb-3 flex-grow">
                                        <Label htmlFor="description">Description</Label>
                                        <Textarea
                                            id="description"
                                            name="description"
                                            value={data.description}
                                            className="mt-1 block w-full"
                                            placeholder="Type your message here."
                                            onChange={(e) => setData('description', e.target.value)}
                                        />
                                    </div>
                                    <div>
                                        {newList.map((item, index) => (
                                            <div key={index} className="hidden">
                                                <Input
                                                    id={`data[${index}].people_id`}
                                                    type="text"
                                                    name={`data[${index}].people_id`}
                                                    value={item.people_id}
                                                    className="mt-1 block w-full"
                                                    autoComplete={`data[${index}].people_id`}
                                                    onChange={(e) => setData('details', e.target.value)}
                                                />

                                                <Input
                                                    id={`data[${index}].distance`}
                                                    type="text"
                                                    name={`data[${index}].distance`}
                                                    value={item.distance}
                                                    className="mt-1 block w-full"
                                                    autoComplete={`data[${index}].distance`}
                                                    onChange={(e) => setData('details', e.target.value)}
                                                />

                                                <Input
                                                    id={`data[${index}].confidence_percentage`}
                                                    type="text"
                                                    name={`data[${index}].confidence_percentage`}
                                                    value={item.confidence_percentage}
                                                    className="mt-1 block w-full"
                                                    autoComplete={`data[${index}].confidence_percentage`}
                                                    onChange={(e) => setData('details', e.target.value)}
                                                />
                                            </div>
                                        ))}
                                    </div>
                                    <div className="flex justify-end">
                                        {loading ? (
                                            <Button type="button" size="sm">
                                                <LoaderCircle className="animate-spin" />
                                                Processing ...
                                            </Button>
                                        ) : (
                                            <Button type="submit" size="sm">Draft Report</Button>
                                        )}
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div className="relative flex h-full min-h-[50vh] flex-col rounded-xl p-4 lg:col-span-2">
                            <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-4">
                                {identify_response.results.map((item, index) => (
                                    <Card key={index} className="max-w-sm mx-auto">
                                        <CardContent className="flex aspect-square items-start justify-center p-6">
                                            <img
                                                alt="Product image"
                                                className="aspect-square object-cover rounded-lg overflow-hidden"
                                                src={item.image_url}
                                            />
                                        </CardContent>
                                        <CardHeader className="text-center">
                                            <CardDescription className="font-semibold text-black text-xl">Name:</CardDescription>
                                            <CardDescription className="font-semibold text-black text-xl">{item.name}</CardDescription>
                                            <CardDescription className="text-lg">Distance:</CardDescription>
                                            <CardDescription className="text-lg">{item.distance}</CardDescription>
                                            <CardDescription className="text-lg">Similarity:</CardDescription>
                                            <CardDescription className="text-lg">{item.confidence_percentage.toFixed(2)} %</CardDescription>
                                        </CardHeader>
                                        <CardContent className="text-3xl font-semibold flex items-center justify-center">
                                            {item.price}
                                        </CardContent>
                                    </Card>
                                ))}
                            </div>
                        </div>
                    </div>
                </form>
            </Authenticated>
        </>
    )
}
import { Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle } from "@/Components/ui/card";
import { Head, router } from "@inertiajs/react";
import { useEffect } from "react";

export default function Generate(props) {
    const { report } = props;

    const imageUrl = report.image_url;
    const reportTitle = report.title;
    const reportDescription = report.description;
    const user = report.user.name;
    const prompt = report.prompt;
    const parts = prompt.split(',');

    // Split each string in the array by commas and flatten the array
    let flattenedArr = parts.map(item => item.split(',')).flat();

    // Join elements from index 16 onwards
    let joinedString = flattenedArr.slice(16).join(', ');

    // Create the final array with the first 16 elements plus the joined string
    let finalArr = flattenedArr.slice(0, 16).concat(joinedString);

    // Parse the date string to a Date object
    const dateObj = new Date(report.created_at);

    // Define an array of month names
    const monthNames = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    // Extract day, month, and year
    const day = String(dateObj.getUTCDate()).padStart(2, '0');
    const monthName = monthNames[dateObj.getUTCMonth()]; // Get the month name
    const year = dateObj.getUTCFullYear();

    // Format the date as dd Month yyyy
    const formattedDate = `${day} ${monthName} ${year}`;

    const headers = [
        "Gender",
        "Age",
        "Skin type",
        "Face shape",
        "Hair",
        "Forehead shape",
        "Eyebrow",
        "Eyes shape",
        "Eyes color",
        "Nose",
        "Lip",
        "Chin",
        "Ears",
        "Additional"
    ];

    useEffect(() => {
        const handlePrint = () => {
            window.print();
        };

        const handleAfterPrint = () => {
            router.visit(route("report.index"), {
                method: "get",
                preserveState: true,
                preserveScroll: true,
            });
        };

        window.addEventListener("afterprint", handleAfterPrint);

        handlePrint();

        return () => {
            window.removeEventListener("afterprint", handleAfterPrint);
        };
    }, []);

    return (
        <>
            <Head title="Report Generate" />

            <div className="flex justify-center">
                <div className="w-full max-w-3xl border border-gray-300 rounded-lg p-6 relative">
                    <div className="text-center mb-4">
                        <h1 className="text-lg md:text-xl font-semibold">FACE IDENTIFICATION REPORT FORM</h1>
                    </div>
                    <div className="absolute top-0 right-0 mt-2 mr-2 bg-green-100 border border-green-500 text-green-700 px-2 py-1 rounded">
                        APPROVED
                    </div>
                    <div className="grid grid-cols-4 gap-8">
                        <div className="col-span-1">
                            <img
                                src={imageUrl}
                                alt="Generated Image"
                                className="w-48 h-48 object-contain"
                            />
                        </div>
                        <div className="col-span-3">
                            <div className="flex flex-col gap-4">
                                <div className="grid grid-cols-2">
                                    <div className="col-span-2">
                                        <CardTitle className="text-base">{reportTitle}</CardTitle>
                                        <CardDescription className="text-sm">{reportDescription}</CardDescription>
                                    </div>
                                    <div className="col-span-1 mt-2">
                                        <CardDescription>Id: {report.id}</CardDescription>
                                        <CardDescription>Date: {formattedDate}</CardDescription>
                                    </div>
                                </div>
                                <table className="w-full">
                                    <tbody>
                                        {headers.map((header, index) => {
                                            if (index < finalArr.length - 3) {
                                                return (
                                                    <tr key={index} className="border-b border-gray-200 last:border-none">
                                                        <td className="py-2 pr-4 font-semibold text-sm text-left">{header}</td>
                                                        <td className="py-2 text-sm">:</td>
                                                        <td className="py-2 text-sm text-left">{finalArr[index + 3]}</td>
                                                    </tr>
                                                );
                                            }
                                            return null;
                                        }).filter(row => row !== null)}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {/* <div className="fixed bottom-4 right-4">
                        <div className="w-48 h-20 text-center">
                            <p>Officer: {report.user.name}</p>
                        </div>
                    </div> */}
                </div>
            </div>

            <div className="my-8 w-full max-w-3xl text-center">
                The face of suspect X, who is suspected to be involved in the criminal incident, was identified. Witnesses saw and correctly stated that this image bears resemblance to the person described by their characteristics. Below is a list of individuals who have similarities with the characteristics of suspect X.
            </div>

            <div className="mt-8 w-full max-w-3xl">
                <div className="grid grid-cols-3 gap-6">
                    {report.ai_analysis.map((item, index) => (
                        <div key={index} className="rounded-lg shadow-md overflow-hidden">
                            <img
                                alt="Similarity Image"
                                className="w-full aspect-square object-cover"
                                src={item.people.image_url}
                            />
                            <div className="p-4 text-center">
                                <p className="font-semibold">{item.people.name}</p>
                                <p>age: {item.people.age}</p>
                                <p>gender: {item.people.gender}</p>
                                <p>similarity: {parseFloat(item.confidence_percentage).toFixed(2)} %</p>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
}
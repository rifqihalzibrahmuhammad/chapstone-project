import { ColumnDef } from "@tanstack/react-table";
import { ArrowUpDown, Check, CheckSquare, Edit, Printer, Trash } from "lucide-react";

import DangerButton from "@/Components/DangerButton";
import InputError from "@/Components/InputError";
import Modal from "@/Components/Modal";
import SecondaryButton from "@/Components/SecondaryButton";
import { Button } from "@/Components/ui/button";
import { Input } from "@/Components/ui/input";
import { Label } from "@/Components/ui/label";
import { Link, useForm } from "@inertiajs/react";
import { FormEventHandler, useState } from "react";
import { Tooltip, TooltipContent, TooltipTrigger } from "@/Components/ui/tooltip";

export type Reports = {
    title: string;
    description: string;
    status: string;
};

export const Columns: ColumnDef<Reports>[] = [
    {
        accessorKey: "title",
        header: ({ column }) => (
            <Button
                variant="ghost"
                onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
            >
                Title
                <ArrowUpDown className="ml-2 h-4 w-4" />
            </Button>
        ),
    },
    {
        accessorKey: "description",
        header: "Description",
    },
    {
        accessorKey: "status",
        header: "Status",
    },
    {
        id: "actions",
        header: "Actions",
        cell: ({ row }) => {
            const reports = row.original;
            const [confirmingReportDeletion, setConfirmingReportDeletion] = useState(false);

            const {
                data,
                setData,
                delete: destroy,
                processing,
                reset,
                errors,
            } = useForm({
                id: reports.id,
            });

            const confirmReportDeletion = () => {
                setConfirmingReportDeletion(true);
            };

            const closeModal = () => {
                setConfirmingReportDeletion(false);

                reset();
            };

            const deleteReport: FormEventHandler = (e) => {
                e.preventDefault();

                destroy(route('admin.report.destroy'), {
                    preserveScroll: true,
                    onSuccess: () => closeModal(),
                    onFinish: () => reset(),
                });
            };

            return (
                <>
                    <Link href={route('report.generate', reports.id)}>
                        <Button>
                            Generate & Download
                        </Button>
                    </Link>
                </>
            );
        },
    },
];

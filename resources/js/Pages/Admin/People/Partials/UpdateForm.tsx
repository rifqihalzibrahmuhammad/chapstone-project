import { Link, router, useForm } from '@inertiajs/react';

import InputError from "@/Components/InputError";
import { Button } from "@/Components/ui/button";
import { Input } from "@/Components/ui/input";
import { Label } from "@/Components/ui/label";
import { useState } from 'react';

interface UpdateFormProps {
    initialData: People | null;
}

export const UpdateForm: React.FC<UpdateFormProps> = ({ initialData }) => {
    const { data, setData, post, errors } = useForm({
        id: (initialData && initialData.id) || "",
        name: (initialData && initialData.name) || "",
        gender: (initialData && initialData.gender) || "",
        age: (initialData && initialData.age) || "",
        image: "", // Add file property to state
    });

    // State to hold preview image URL
    const [previewImage, setPreviewImage] = useState<string | null>(null);

    const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const formData = new FormData();
        formData.append('name', data.name);
        formData.append('gender', data.gender);
        formData.append('age', data.age);
        formData.append('image', data.image);

        post(route('admin.people.update'), {
            onSuccess: () => {
                // Optionally, you can add additional logic here if needed
            },
            onError: (errors) => {
                // Handle errors if needed
            },
            data: formData,
            preserveScroll: true,
        });
    };

    // Handle file input change
    const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files[0]) {
            const image = e.target.files[0];
            setData('image', image);
            // Generate preview image URL
            const reader = new FileReader();
            reader.onloadend = () => {
                setPreviewImage(reader.result as string);
            };
            reader.readAsDataURL(image);
        }
    };

    return (
        <form onSubmit={onSubmit} className="space-y-4">
            <div>
                <Label htmlFor="name">Name</Label>
                <Input
                    id="name"
                    type="text"
                    name="name"
                    value={data.name}
                    className="mt-1 block w-full"
                    autoComplete="name"
                    onChange={(e) => setData('name', e.target.value)}
                />
                <InputError message={errors.name} />
            </div>

            <div>
                <Label htmlFor="gender">Gender</Label>
                <Input
                    id="gender"
                    type="text"
                    name="gender"
                    value={data.gender}
                    className="mt-1 block w-full"
                    autoComplete="gender"
                    onChange={(e) => setData('gender', e.target.value)}
                />
                <InputError message={errors.gender} />
            </div>

            <div>
                <Label htmlFor="age">Age</Label>
                <Input
                    id="age"
                    type="number"
                    name="age"
                    value={data.age}
                    className="mt-1 block w-full"
                    autoComplete="age"
                    onChange={(e) => setData('age', e.target.value)}
                />
                <InputError message={errors.age} />
            </div>

            <div>
                <Label htmlFor="image">Face image</Label>
                <Input
                    type="file"
                    id="image"
                    accept="image/*"
                    onChange={handleImageChange} // Call handleFileChange on file input change
                />
                {previewImage && <img src={previewImage} alt="Preview" className="mt-2 max-w-full h-auto" />}
            </div>

            <div className="flex gap-1 justify-end">
                <Link href={route('admin.people.index')}>
                    <Button type="button" variant="outline">
                        Back
                    </Button>
                </Link>
                <Button type="submit">Save</Button>
            </div>
        </form>
    )
}

import Admin from "@/Layouts/AdminLayout"
import { Head } from "@inertiajs/react"
import { CreateForm } from "./Partials/CreateForm"
import { UpdateForm } from "./Partials/UpdateForm"

export default function Index(props) {
    const { people } = props

    return (
        <>
            <Head title="Admin | People Form" />

            <Admin>
                <div className="flex items-center">
                    <h1 className="text-lg font-semibold md:text-2xl">People Form</h1>
                </div>
                <div className="flex flex-1 justify-center rounded-lg border border-dashed shadow-sm">
                    <div className="flex flex-col gap-1 w-full p-4">
                        {people ? <UpdateForm initialData={people} /> : <CreateForm />}
                    </div>
                </div>
            </Admin>
        </>
    )
}
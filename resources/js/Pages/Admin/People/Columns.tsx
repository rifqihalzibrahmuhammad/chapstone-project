import { ColumnDef } from "@tanstack/react-table";
import { ArrowUpDown, Edit, Trash } from "lucide-react";

import DangerButton from "@/Components/DangerButton";
import InputError from "@/Components/InputError";
import Modal from "@/Components/Modal";
import SecondaryButton from "@/Components/SecondaryButton";
import { Button } from "@/Components/ui/button";
import { Input } from "@/Components/ui/input";
import { Label } from "@/Components/ui/label";
import { Link, useForm } from "@inertiajs/react";
import { FormEventHandler, useState } from "react";
import { Dialog, DialogContent, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "@/Components/ui/dialog";
import { Tooltip, TooltipContent, TooltipTrigger } from "@/Components/ui/tooltip";

export type Peoples = {
    name: string;
    gender: string;
    age: number;
};

export const Columns: ColumnDef<Peoples>[] = [
    {
        accessorKey: "name",
        header: ({ column }) => (
            <Button
                variant="ghost"
                onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
            >
                Name
                <ArrowUpDown className="ml-2 h-4 w-4" />
            </Button>
        ),
    },
    {
        accessorKey: "gender",
        header: "Gender",
    },
    {
        accessorKey: "age",
        header: "Age",
    },
    {
        id: "actions",
        header: "Actions",
        cell: ({ row }) => {
            const people = row.original;
            const [confirmingPeopleDeletion, setConfirmingPeopleDeletion] = useState(false);

            const {
                data,
                setData,
                delete: destroy,
                processing,
                reset,
                errors,
            } = useForm({
                id: people.id,
            });

            const confirmPeopleDeletion = () => {
                setConfirmingPeopleDeletion(true);
            };

            const closeModal = () => {
                setConfirmingPeopleDeletion(false);

                reset();
            };

            const onDelete: FormEventHandler = (e) => {
                e.preventDefault();

                destroy(route('admin.people.destroy'), {
                    preserveScroll: true,
                    onSuccess: () => closeModal(),
                    onFinish: () => reset(),
                });
            };

            return (
                <>
                    <Tooltip>
                        <TooltipTrigger asChild>
                            <Link href={route('admin.people.edit', people.id)}>
                                <Button
                                    variant="ghost"
                                    size="icon"
                                >
                                    <Edit className="size-5" />
                                </Button>
                            </Link>
                        </TooltipTrigger>
                        <TooltipContent side="right" sideOffset={5}>
                            Edit
                        </TooltipContent>
                    </Tooltip>

                    <Dialog>
                        <DialogTrigger asChild>
                            <Button
                                variant="ghost"
                                size="icon"
                            >
                                <Trash className="size-5" />
                            </Button>
                        </DialogTrigger>
                        <DialogContent>
                            <DialogHeader>
                                <DialogTitle>
                                    Are you sure you want to delete this person?
                                </DialogTitle>
                            </DialogHeader>
                            <form onSubmit={onDelete}>
                                Once this person is deleted, all of its resources and data will be permanently deleted. Please
                                enter your password to confirm you would like to permanently delete this person.

                                <div className="mt-6 hidden">
                                    <Label htmlFor="name">Id</Label>
                                    <Input
                                        id="id"
                                        type="text"
                                        name="id"
                                        value={data.id}
                                        className="mt-1 block w-full"
                                        autoComplete="id"
                                        onChange={(e) => setData('id', e.target.value)}
                                    />
                                    <InputError message={errors.id} />
                                </div>

                                <DialogFooter className="mt-6">
                                    <DangerButton disabled={processing}>
                                        Delete Person
                                    </DangerButton>
                                </DialogFooter>
                            </form>
                        </DialogContent>
                    </Dialog>
                </>
            );
        },
    },
];

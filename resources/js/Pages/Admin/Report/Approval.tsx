import { Button } from "@/Components/ui/button"
import { Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle } from "@/Components/ui/card"
import { Label } from "@/Components/ui/label"
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/Components/ui/select"
import { Table, TableBody, TableCell, TableRow } from "@/Components/ui/table"
import Admin from "@/Layouts/AdminLayout"
import { Head, Link, useForm } from "@inertiajs/react"

export default function Approval(props) {
    const { report } = props

    const imageUrl = report.image_url
    const reportTitle = report.title
    const reportDescription = report.description
    const user = report.user.name
    const prompt = report.prompt
    const parts = prompt.split(',');

    // Split each string in the array by commas and flatten the array
    let flattenedArr = parts.map(item => item.split(',')).flat();

    // Join elements from index 16 onwards
    let joinedString = flattenedArr.slice(16).join(', ');

    // Create the final array with the first 16 elements plus the joined string
    let finalArr = flattenedArr.slice(0, 16).concat(joinedString);

    const headers = [
        "Gender",
        "Age",
        "Skin type",
        "Face shape",
        "Hair",
        "Forehead shape",
        "Eyebrow",
        "Eyes shape",
        "Eyes color",
        "Nose",
        "Lip",
        "Chin",
        "Ears",
        "Additional"
    ];

    const { data, setData, patch, errors } = useForm({
        status: (report && report.status) || "",
    });

    const approve = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        patch(route('admin.report.update', report.id), {
            onSuccess: () => {
                console.log("Form submitted successfully");
                // Handle success actions here
            },
            onError: (errors) => {
                console.error("Form submission error:", errors);
                // Handle error actions here
            },
        });
    };

    const handleStatusChange = (value: string) => {
        setData('status', value);
    };

    return (
        <>
            <form onSubmit={approve} method='PUT'>

                <Head title="Report Approval" />

                <Admin>
                    <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-12">
                        <div className="flex items-center mb-8 justify-between">
                            <h1 className="text-2xl font-semibold md:text-3xl">Report Detail</h1>
                            <div className="flex gap-1 justify-end">
                                <Link href={route('admin.report.index')}>
                                    <Button type="button" variant="outline">
                                        Back
                                    </Button>
                                </Link>
                                <Button type="submit">Save</Button>
                            </div>
                        </div>
                        <div className="grid grid-cols-1 lg:grid-cols-4 gap-8">
                            <div className="lg:col-span-1">
                                <img
                                    src={imageUrl}
                                    alt="Generated Image"
                                    className="w-full aspect-square rounded-lg shadow-md"
                                />
                            </div>
                            <div className="lg:col-span-3">
                                <Card className="flex flex-col gap-4 p-6 rounded-lg shadow-md">
                                    <CardHeader className="grid grid-cols-1 lg:grid-cols-4">
                                        <div className="col-span-3">
                                            <CardTitle>{reportTitle}</CardTitle>
                                            <CardDescription>{reportDescription}</CardDescription>
                                        </div>
                                        <div className="col-span-1">
                                            <Select value={data.status} onValueChange={handleStatusChange}>
                                                <SelectTrigger>
                                                    <SelectValue placeholder="Select status" />
                                                </SelectTrigger>
                                                <SelectContent>
                                                    <SelectItem value="approve">Approve</SelectItem>
                                                    <SelectItem value="reject">Reject</SelectItem>
                                                </SelectContent>
                                            </Select>
                                        </div>
                                    </CardHeader>
                                    <CardContent>
                                        <table className="w-full">
                                            <tbody>
                                                {headers.map((header, index) => {
                                                    if (index < finalArr.length - 3) {
                                                        return (
                                                            <tr key={index} className="border-b border-gray-200 last:border-none">
                                                                <td className="py-2 pr-4 font-semibold text-left">{header}</td>
                                                                <td className="py-2">:</td>
                                                                <td className="py-2 text-left">{finalArr[index + 3]}</td>
                                                            </tr>
                                                        );
                                                    }
                                                    return null;
                                                }).filter(row => row !== null)}
                                            </tbody>
                                        </table>
                                    </CardContent>
                                    <CardFooter>
                                        {/* Footer content if any */}
                                    </CardFooter>
                                </Card>
                            </div>
                        </div>
                        <div className="mt-8">
                            <div className="text-center text-xl font-semibold mb-4">Similarity</div>
                            <div className="grid grid-cols-1 lg:grid-cols-3 gap-6">
                                {report.ai_analysis.map((item, index) => (
                                    <div key={index} className="rounded-lg shadow-md overflow-hidden">
                                        <img
                                            alt="Similarity Image"
                                            className="w-full aspect-square object-cover"
                                            src={item.people.image_url}
                                        />
                                        <div className="p-4 text-center">
                                            <p className="font-semibold">{item.people.name}</p>
                                            <p>age: {item.people.age}</p>
                                            <p>gender: {item.people.gender}</p>
                                            <p>similarity: {parseFloat(item.confidence_percentage).toFixed(2)}</p>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </Admin>
            </form>
        </>
    )
}
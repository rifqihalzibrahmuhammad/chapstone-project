import Admin from "@/Layouts/AdminLayout"
import { Head } from "@inertiajs/react"

export default function Index() {
    return (
        <>
            <Head title="Admin | Dashboard" />

            <Admin>
                <div className="flex items-center">
                    <h1 className="text-lg font-semibold md:text-2xl">Dashboard</h1>
                </div>
                <div className="flex flex-1 justify-center rounded-lg border border-dashed shadow-sm">
                    <div className="flex flex-col gap-1 w-full p-4">

                    </div>
                </div>
            </Admin>
        </>
    )
}
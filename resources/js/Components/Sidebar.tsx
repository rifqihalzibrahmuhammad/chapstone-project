import {
    Fingerprint,
    GitFork,
    GitGraph,
    GitPullRequest,
    SquareUser
} from "lucide-react"

import { Button } from "@/Components/ui/button"
import {
    Tooltip,
    TooltipContent,
    TooltipTrigger,
} from "@/Components/ui/tooltip"

import { cn } from "@/lib/utils"

import { Link, usePage } from "@inertiajs/react"

export default function Sidebar() {
    const { url } = usePage(); // Get the current route URL
    const pathname = url.split('?')[0]; // Remove any query parameters, keeping only the path

    return (
        <aside className="inset-y fixed  left-0 z-20 flex h-full flex-col border-r">
            <div className="border-b p-2">
                <Link href={route('dashboard.index')}>
                    <Button variant="outline" size="icon" aria-label="Home">
                        <Fingerprint className="size-5" />
                    </Button>
                </Link>
            </div>
            <nav className="grid gap-1 p-2">
                <Tooltip>
                    <TooltipTrigger asChild>
                        <Link href={route('dashboard.index')}>
                            <Button
                                variant="ghost"
                                size="icon"
                                className={cn(
                                    'rounded-lg',
                                    pathname === '/' && 'bg-muted' // Add the 'bg-muted' class if it's the current page
                                )}
                                aria-label="Dashboard"
                            >
                                <GitFork className="size-5" />
                            </Button>
                        </Link>
                    </TooltipTrigger>
                    <TooltipContent side="right" sideOffset={5}>
                        Dashboard
                    </TooltipContent>
                </Tooltip>
                <Tooltip>
                    <TooltipTrigger asChild>
                        <Link href={route('generate.index')}>
                            <Button
                                variant="ghost"
                                size="icon"
                                className={cn(
                                    'rounded-lg',
                                    pathname === '/generate' && 'bg-muted' // Add the 'bg-muted' class if it's the current page
                                )}
                                aria-label="Generate"
                            >
                                <GitPullRequest className="size-5" />
                            </Button>
                        </Link>
                    </TooltipTrigger>
                    <TooltipContent side="right" sideOffset={5}>
                        Generate
                    </TooltipContent>
                </Tooltip>
                <Tooltip>
                    <TooltipTrigger asChild>
                        <Link href={route('report.index')}>
                            <Button
                                variant="ghost"
                                size="icon"
                                className={cn(
                                    'rounded-lg',
                                    pathname === '/report' && 'bg-muted' // Add the 'bg-muted' class if it's the current page
                                )}
                                aria-label="Models"
                            >
                                <GitGraph className="size-5" />
                            </Button>
                        </Link>
                    </TooltipTrigger>
                    <TooltipContent side="right" sideOffset={5}>
                        Report
                    </TooltipContent>
                </Tooltip>
            </nav>
            <nav className="mt-auto grid gap-1 p-2">
                <Tooltip>
                    <TooltipTrigger asChild>
                        <Link href={route('admin.dashboard.index')}>
                            <Button
                                variant="ghost"
                                size="icon"
                                className="mt-auto rounded-lg"
                                aria-label="Admin"
                            >
                                <SquareUser className="size-5" />
                            </Button>
                        </Link>
                    </TooltipTrigger>
                    <TooltipContent side="right" sideOffset={5}>
                        Admin
                    </TooltipContent>
                </Tooltip>
            </nav>
        </aside>
    )
}

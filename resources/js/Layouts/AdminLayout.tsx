import Master from "./MasterLayout"

import AdminResponsiveSidebar from "@/Components/AdminResponsiveSidebar"
import AdminSidebar from "@/Components/AdminSidebar"
import { TooltipProvider } from "@/Components/ui/tooltip"
import { PropsWithChildren } from "react"

export default function Admin({ children }: PropsWithChildren) {
    return (
        <>
            <Master>
                <TooltipProvider>
                    <div className="grid min-h-screen w-full md:grid-cols-[220px_1fr] lg:grid-cols-[280px_1fr]">
                        <AdminSidebar />
                        <div className="flex flex-col">
                            <AdminResponsiveSidebar />
                            <main className="flex flex-1 flex-col gap-4 p-4 lg:gap-6 lg:p-6">
                                {children}
                            </main>
                        </div>
                    </div>
                </TooltipProvider>
            </Master>
        </>
    )
}
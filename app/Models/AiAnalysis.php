<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AiAnalysis extends Model
{
    use HasFactory;

    protected $table = 'ai_analysis';
    protected $fillable = [
        'id',
        'report_id',
        'people_id',
        'distance',
        'confidence_percentage',
    ];

    public function report(): BelongsTo
    {
        return $this->belongsTo(Report::class);
    }

    public function people(): BelongsTo
    {
        return $this->belongsTo(People::class);
    }
}

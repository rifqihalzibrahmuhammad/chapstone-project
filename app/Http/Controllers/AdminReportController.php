<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class AdminReportController extends Controller
{
    public function index(): Response
    {
        $reports = Report::all()->map(function ($report) {
            return [
                'id' => $report->id,
                'title' => $report->title,
                'description' => $report->description,
                'status' => $report->status,
            ];
        });

        return Inertia::render('Admin/Report/Index', [
            'reports' => $reports,
        ]);
    }

    public function approve(string $id): Response
    {
        $report = Report::with(['user', 'aiAnalysis.people'])->findOrFail($id);
        return Inertia::render('Admin/Report/Approval', [
            'report' => $report
        ]);
    }

    public function update(Request $request, string $id): RedirectResponse
    {
        $validatedData = $request->validate([
            'status' => 'required|string',
        ]);

        $report = Report::findOrFail($id);

        $report->status = $validatedData['status'];

        $report->save();

        return redirect()->intended(route('admin.report.index', [], false));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\People;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class AdminPeopleController extends Controller
{
    public function index(): Response
    {
        $peoples = People::all()->map(function ($people) {
            return [
                'id' => $people->id,
                'name' => $people->name,
                'gender' => $people->gender,
                'age' => $people->age,
            ];
        });

        $create_url = route('admin.people.create');

        return Inertia::render('Admin/People/Index', [
            'peoples' => $peoples,
            'create_url' => $create_url,
        ]);
    }

    public function create()
    {
        return Inertia::render('Admin/People/Form');
    }

    public function edit(string $id): Response
    {
        $people = People::findOrFail($id);
        return Inertia::render('Admin/People/Form', [
            'people' => $people
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'gender' => 'required|string',
            'age' => 'required|numeric',
            'image' => 'required|image',
        ]);

        $image = $request->file('image');

        $response = Http::asMultipart()->attach(
            'image',
            file_get_contents($image->getRealPath()),
            $image->getClientOriginalName()
        )->post('http://127.0.0.1:8000/api/create-people', [
            'name' => $validatedData['name'],
            'gender' => $validatedData['gender'],
            'age' => $validatedData['age'],
        ]);

        if ($response->successful()) {
            // Handle successful response
            return redirect()->route('admin.people.index')->with('success', 'People created successfully');
        } else {
            // Handle error response
            return redirect()->back()->withErrors(['error' => 'Failed to create people']);
        }
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer',
            'name' => 'required|string',
            'gender' => 'required|string',
            'age' => 'required|integer',
            'image' => 'nullable|image',
        ]);

        $image = $request->file('image');

        $response = Http::asMultipart()->attach(
            'image',
            file_get_contents($image->getRealPath()),
            $image->getClientOriginalName()
        )->put('http://127.0.0.1:8000/api/update-people/' . $validatedData['id'], [
            'name' => $validatedData['name'],
            'gender' => $validatedData['gender'],
            'age' => $validatedData['age'],
        ]);

        if ($response->successful()) {
            // Handle successful response
            return redirect()->route('admin.people.index')->with('success', 'People created successfully');
        } else {
            // Handle error response
            return redirect()->back()->withErrors(['error' => 'Failed to create people']);
        }
    }

    public function destroy(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'id' => 'required',
        ]);

        $people = People::findOrFail($validatedData['id']);

        $people->delete();

        return redirect()->intended(route('admin.people.index', [], false));
    }
}

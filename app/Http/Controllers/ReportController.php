<?php

namespace App\Http\Controllers;

use App\Models\AiAnalysis;
use App\Models\Report;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Inertia\Response;

class ReportController extends Controller
{
    public function index(): Response
    {
        $reports = Report::all()->map(function ($report) {
            return [
                'id' => $report->id,
                'title' => $report->title,
                'description' => $report->description,
                'status' => $report->status,
            ];
        });

        return Inertia::render('User/Report/Index', [
            'reports' => $reports,
        ]);
    }

    public function store(Request $request)
    {
        try {
            // Validate the request
            $validatedData = $request->validate([
                'image' => 'required|image',
                'prompt' => 'required|string',
                'title' => 'required|string',
                'description' => 'required|string',
                'details' => 'required|array'
            ]);

            $imageUrl = '';

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = time() . $file->getClientOriginalExtension();
                // Handle file upload to AWS S3
                Storage::disk('s3')->put('generated_sketch/' . $filename, file_get_contents($file), 'public');
                $imageUrl = Storage::disk('s3')->url('generated_sketch/' . $filename);
            } else {
                throw new \Exception("Image is required.");
            }

            // Create the report record in the database
            $report = Report::create([
                'user_id' => Auth::id(),
                'title' => $validatedData['title'],
                'description' => $validatedData['description'],
                'prompt' => $validatedData['prompt'],
                'image_url' => $imageUrl, // Save the URL in the database
                'status' => 'pending',
            ]);

            // Create AiAnalysis records for each detail
            foreach ($request->details as $detail) {
                AiAnalysis::create([
                    'report_id' => $report->id,
                    'people_id' => $detail['people_id'],
                    'distance' => $detail['distance'],
                    'confidence_percentage' => $detail['confidence_percentage'],
                ]);
            }

            // Redirect to the index route
            return redirect()->route('report.index');
        } catch (\Exception $e) {
            // Handle exceptions and validation errors
            return redirect()->back()->withErrors([$e->getMessage()]);
        }
    }

    public function generate(string $id): Response
    {
        $report = Report::with(['user', 'aiAnalysis.people'])->findOrFail($id);
        return Inertia::render('User/Report/Generate', [
            'report' => $report
        ]);
    }
}
